resource "azurerm_resource_group" "rg" {
    name= "${var.name_rg}"
    location = "${var.location}"

    tags {
        owner= "${var.owner}"
    }
}

resource "azurerm_virtual_network" "vNet" {
    name= "${var.name_vnet}"
    address_space= "${var.address_space}"
    location= "${azurerm_resource_group.rg.location}"
    resource_group_name= "${azurerm_resource_group.rg.name}"  
}

resource "azurerm_subnet" "subNet" {
    name= "${var.name_subNet}"
    resource_group_name= "${azurerm_resource_group.rg.name}" 
    virtual_network_name= "${azurerm_virtual_network.vNet.name}"
    address_prefix= "${var.address_prefix}"
}

resource "azurerm_network_security_group" "NSecure" {
    name= "${var.name_secure}"
    location= "${azurerm_resource_group.rg.location}"
    resource_group_name= "${azurerm_resource_group.rg.name}"  

    security_rule {
        name= "SSH"
        priority= "1001"
    #incrémenter à chaque création de port"
        direction= "Inbound"
        access= "Allow"
        protocol= "Tcp"
        source_port_range= "*"
        destination_port_range= "22"
        source_address_prefix= "*"
        destination_address_prefix= "*"
    }
    security_rule {
        name= "HTTP"
        priority= "1002"
        direction= "Inbound"
        access= "Allow"
        protocol= "Tcp"
        source_port_range= "*"
        destination_port_range= "80"
        source_address_prefix= "*"
        destination_address_prefix= "*"
    }
}

resource "azurerm_public_ip" "IP" {
    name= "${var.name_IP}"
    location= "${azurerm_resource_group.rg.location}"
    resource_group_name= "${azurerm_resource_group.rg.name}"  
    allocation_method= "Static"
}

resource "azurerm_lb" "test" {
    name= "loadBalancer"
    location= "${azurerm_resource_group.rg.location}"
    resource_group_name= "${azurerm_resource_group.rg.name}"  

    frontend_ip_configuration {
    name= "pubIP"
    public_ip_address_id= "${azurerm_public_ip.IP.id}"
    }
}

resource "azurerm_lb_rule" "example" {
  resource_group_name= "${azurerm_resource_group.rg.name}"  
  loadbalancer_id= "${azurerm_lb.test.id}"
  name= "SSH"
  protocol= "Tcp"
  frontend_port= "22"
  backend_port= "22"
  backend_address_pool_id= "${azurerm_lb_backend_address_pool.test.id}"
  frontend_ip_configuration_name= "pubIP"
  probe_id=  "${azurerm_lb_probe.example.id}"
}


resource "azurerm_lb_probe" "example" {
  resource_group_name= "${azurerm_resource_group.rg.name}"  
  loadbalancer_id= "${azurerm_lb.test.id}"
  name= "ssh-running-probe"
  port= "22"
}

resource "azurerm_lb_backend_address_pool" "test" {
 resource_group_name= "${azurerm_resource_group.rg.name}" 
 loadbalancer_id= "${azurerm_lb.test.id}"
 name= "BackEndAddressPool"
}

resource "azurerm_network_interface" "NIC" {
  count= 2
  name= "${var.name_ni}_${count.index}"
  location= "${azurerm_resource_group.rg.location}"
  resource_group_name = "${azurerm_resource_group.rg.name}" 
#   network_security_group_id= "${azurerm_network_security_group.NSecure.id}"

  ip_configuration {
    name= "${var.ip_config}"
    subnet_id= "${azurerm_subnet.subNet.id}"
    private_ip_address_allocation = "Dynamic"
    # public_ip_address_id= "${azurerm_public_ip.IP.id}"
    load_balancer_backend_address_pools_ids = [ "${azurerm_lb_backend_address_pool.test.id}" ]
  }
}

resource "azurerm_managed_disk" "test" {
 count= 2
 name= "disk_${count.index}"
 location= "${azurerm_resource_group.rg.location}"
 resource_group_name= "${azurerm_resource_group.rg.name}"
 storage_account_type= "Standard_LRS"
 create_option= "Empty"
 disk_size_gb= "2048"
}

resource "azurerm_availability_set" "avset" {
 name= "avset"
 location= "${azurerm_resource_group.rg.location}"
 resource_group_name= "${azurerm_resource_group.rg.name}"
 platform_fault_domain_count= 2
 platform_update_domain_count= 2
 managed= true
}

resource "azurerm_virtual_machine" "VM" {
  count= 2
  name= "${var.name_vm}_${count.index}"
  location= "${azurerm_resource_group.rg.location}"
  availability_set_id= "${azurerm_availability_set.avset.id}"
  resource_group_name= "${azurerm_resource_group.rg.name}"  
  network_interface_ids= [ "${element(azurerm_network_interface.NIC.*.id, count.index)}" ]
  vm_size= "${var.vmSize}"

  storage_image_reference {
    publisher= "OpenLogic"
    offer= "CentOS"
    sku= "7.5"
    version= "latest"
  }

  storage_os_disk {
    name= "osdisk_${count.index}"
    caching= "ReadWrite"
    create_option= "FromImage"
    managed_disk_type= "Standard_LRS"
  }

  storage_data_disk {
   name= "${element(azurerm_managed_disk.test.*.name, count.index)}"
   managed_disk_id = "${element(azurerm_managed_disk.test.*.id, count.index)}"
   create_option   = "Attach"
   lun             = 1
   disk_size_gb    = "${element(azurerm_managed_disk.test.*.disk_size_gb, count.index)}"
  }

  os_profile {
    computer_name= "pepette"
    admin_username= "estelle"
  }

  os_profile_linux_config {
      disable_password_authentication= true

      ssh_keys {
          path= "/home/estelle/.ssh/authorized_keys"
          key_data= "${var.key_data}"
      }
  }
}